#!/bin/bash
#### Name:      jboss_start.sh
#### Date:      2021-09-10
#### Version:   1.0.0
#### Author :   Subbu
#### Description:  Script to Start JBOSS Server

#Defining JBOSS_HOME
JBOSS_HOME=/t24disk/R21MB/jboss-eap-7.2

# Cleaning the nohup.out file
cd $JBOSS_HOME/bin
rm -rf nohup.out

# Cleaning the log,date and tmp folder
cd $JBOSS_HOME/standalone
rm -rf log data tmp

# Setting up an Environment Variable
cd $JBOSS_HOME/bin
export JAVA_HOME=/t24disk/R21MB/java/jdk1.8.0_291/
export TAFJ_HOME=/t24disk/R21MB/TAFJ

#---Start JBOSS---#
hostIP=`hostname -i`
nohup sh standalone.sh --server-config=standalone-full-ha.xml -Dtemenos.log.directory='/var/log/app' -Dtemenos.log.directory.t24='/var/log/app' -Dtemenos.log.directory.como='/var/log/app' -Djboss.server.log.dir='/var/log/app' -b $hostIP > nohup.out 2>&1 &
echo "JBoss Application Server is started successfully"
