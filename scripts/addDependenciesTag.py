#!/usr/bin/env python

"""addDependenciesTag.py: Python Program to create dependencies txt file with t24 dependency information."""
__author__      = "Subbu"
__version__     = "1.0.0"
__maintainer__  = "Subbu"
__email__       = "subramani.a.murugesan@capgemini.com"

import os
path = input("Enter t24lib path: ")
version= input("Enter t24ReleaseVersion: ")
lst = []
dir_list = os.listdir(path)
for x in dir_list:
    lst.append(x.split('.')[0])
str = ""
for i in lst:
    str +="<dependency>\n\
        <groupId>com.temenos</groupId>\n \
        <artifactId>"+i+".jar</artifactId>\n \
        <version>"+version+"</version>\n \
        </dependency>\n"
text_file = open("dependencies.txt", "w")
text_file.write(str)
text_file.close()