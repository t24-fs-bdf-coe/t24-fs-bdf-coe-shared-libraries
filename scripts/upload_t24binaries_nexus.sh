#!/bin/bash
### BEGIN INIT INFO
# Name        : upload_t24binaries_nexus.sh
# Author      : Subbu
# Date        : 2021-08-23
# Version     : 1.0.0
# Description : Script to upload T24 binary component to Nexus Repo.
### END INIT INFO

# Function to read filePath,releaseVersion,groupId from property file and upload T24 binary component to Nexus Repo.
# Usage: upload_t24binaries_nexus.sh
#

set +e
set -o noglob

#
# Set Colors
#

bold="\e[1m"
dim="\e[2m"
underline="\e[4m"
blink="\e[5m"
reset="\e[0m"
red="\e[31m"
green="\e[32m"
blue="\e[34m"


#
# Common Output Styles
#

h1() {
  printf "\n${bold}${underline}%s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
h2() {
  printf "\n${bold}%s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
info() {
  printf "${dim}➜ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
success() {
  printf "${green}✔ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
error() {
  printf "${red}${bold}✖ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
warnError() {
  printf "${red}✖ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
warnNotice() {
  printf "${blue}✖ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
note() {
  printf "\n${bold}${blue}Note:${reset} ${blue}%s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}

#
# Common variables 
#

#Defining T24_RELEASE_PROPERTY_FILE_PATH
T24_RELEASE_PROPERTY_FILE_PATH=./T24.RELEASE.properties

# Runs the specified command and logs it appropriately.
#   $1 = command
#   $2 = (optional) error message
#   $3 = (optional) success message
#   $4 = (optional) global variable to assign the output to


function upload_t24binaries_nexus() {
	#Reading filePath from property file
	filePath=$(getValue "filePath")
	echo "filePath is set to : $filePath"
	
	#Reading releaseVersion from property file
	releaseVersion=$(getValue "releaseVersion")
	echo "releaseVersion is set to : $releaseVersion"
	
	#Reading groupId from property file
	groupId=$(getValue "groupId")
	echo "groupId is set to : $groupId"
	
	#Reading Version from property file
	version=$(getValue "version")
	echo "Version is set to : $version"
	
	#Reading nexusRepoID from property file
	nexusRepoID=$(getValue "nexusRepoID")
	echo "nexusRepoID is set to : $nexusRepoID"
	
	#Reading nexusURL from property file
	nexusURL=$(getValue "nexusURL")
	echo "nexusURL is set to : $nexusURL"
	
	cd $filePath
	pwd
	for file in `ls | grep .jar$`; do
		filename="${file%.*}"
		extension="${file##*.}"
		mvn deploy:deploy-file -DgroupId=$groupId.$releaseVersion -DartifactId=$filename -Dversion=$version -DgeneratePom=true -Dpackaging=jar -DrepositoryId=$nexusRepoID -Durl=$nexusURL -Dfile=$file;
	done

}

#Function to get value from t24 release property file
function getValue() {
	PROP_KEY=$1
	PROP_VALUE=`cat $T24_RELEASE_PROPERTY_FILE_PATH | grep "$PROP_KEY" | cut -d'=' -f2`
	echo $PROP_VALUE
}

#Calling upload_t24binaries_nexus function
upload_t24binaries_nexus


