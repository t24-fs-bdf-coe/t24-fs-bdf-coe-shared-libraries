#!/bin/bash

version=$1
commit_type=$(echo $2 | cut -d : -f 1)

major=$(echo $version | cut -d . -f 1)
minor=$(echo $version | cut -d . -f 2)
patch=$(echo $version | cut -d . -f 3 | cut -d - -f 1)

case ${commit_type,,} in

    major)
        major=$((major+1))
        minor="0"
        patch="0"
        ;;

    minor)
        minor=$((minor+1))
        patch="0"
        ;;

    patch)
        patch=$((patch+1))
        ;;

esac

echo "$major.$minor.$patch-SNAPSHOT"
