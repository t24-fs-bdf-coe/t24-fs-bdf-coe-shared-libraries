#!/bin/bash
#### Name:      remove_deployed_artifacts.sh
#### Date:      2021-10-27
#### Version:   1.0.0
#### Author :   Subbu
#### Description:  Script to remove deployed artifacts before deployment
#Defining JBOSS_HOME
JBOSS_HOME=/t24disk/R21MB/jboss-eap-7.2
echo $JBOSS_HOME
cd $JBOSS_HOME/standalone/deployments
if [[ -f t24iris.war ]]; then
   rm -rf t24iris.war 
   rm -rf t24iris.war.deployed 
   rm -rf t24iris.war.failed 
   echo "t24iris.war Removed"
fi
