#!/bin/bash
#### Name    : check_deployment_status.sh
#### Author  : Subbu
#### Date    : 2021-10-27
#### Version : 1.0.0
#### Description:  Shell Script to check the deployment status of T24 IRIS war file

#---Check to see if deployment status is success or failure---#
JBOSS_IP=`hostname -i`
PORT_NO=9990
JBOSS_HOME=/t24disk/R21MB/jboss-eap-7.2
echo $JBOSS_HOME
deploymentStatus=`$JBOSS_HOME/bin/jboss-cli.sh --connect --controller=$JBOSS_IP:$PORT_NO command="/deployment=t24iris.war:read-attribute(name=status)" --timeout=40000`
echo $deploymentStatus
status=`echo $deploymentStatus | grep "success"`
if [ $? != 0 ]
then
        echo "Deployment is failed"
		echo $status
		exit 1
else
		echo "Deployment is Success"
        echo $status	
fi 
