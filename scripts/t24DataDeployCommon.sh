#!/bin/sh

### BEGIN INIT INFO

# Name        : t24dataDeployCommon.sh
# Author      : Subbu
# Email       : subramani.a.murugesan@capgemini.com
# Date        : 2021-10-13
# Version     : 1.0.0
# Description : Script to deploy T24 data components on an as-needed basis

### END INIT INFO
# Function to stop JBOSS AS(Application Server),download t24 artifacts from Nexus to desired location for deployment,cleanup undeployed t24 #modules, deploy t24 component using T24PackageDataInstaller,deploy t24 data record using packageDataInstaller and finally start JBOSS #AS(Application Server) post successful deployment.

# Usage: t24dataDeployment.sh $modulename $version
# Example: t24dataDeployment.sh Customer 1.0.0-SNAPSHOT/1.0.0
#
set +e
set -o noglob

#
# Set Colors
#
bold="\e[1m"
dim="\e[2m"
underline="\e[4m"
blink="\e[5m"
reset="\e[0m"
red="\e[31m"
green="\e[32m"
blue="\e[34m"

#
# Common Output Styles
#

h1() {
  printf "\n${bold}${underline}%s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
h2() {
  printf "\n${bold}%s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
info() {
  printf "${dim}➜ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
success() {
  printf "${green}✔ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
error() {
  printf "${red}${bold}✖ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
warnError() {
  printf "${red}✖ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
warnNotice() {
  printf "${blue}✖ %s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}
note() {
  printf "\n${bold}${blue}Note:${reset} ${blue}%s${reset}\n" "$(echo "$@" | sed '/./,$!d')"
}

#
# Common variables
#
#Defining TAFJ_HOME
TAFJ_HOME=/t24disk/R21MB/TAFJ

#Defining T24_HOME
T24_HOME=/t24disk/R21MB/bnk/UD

#Defining T24_DEPLOY_PATH
T24_DEPLOY_PATH=/t24disk/R21MB/bnk/UD/package

#Defining T24_LIB_PATH
T24_LIB_PATH=/t24disk/R21MB/bnk/locallib

#Defining JBOSS_HOME
JBOSS_HOME=/t24disk/R21MB/jboss-eap-7.2

#Defining JAVA_HOME
JAVA_HOME=/t24disk/R21MB/java/jdk1.8.0_291

#Defining T24_LOCALLIB_PATH
T24_LOCALLIB_PATH=/t24disk/R21MB/bnk/locallib

#Export all Variables
export TAFJ_HOME
export T24_HOME
export JAVA_HOME
export JBOSS_HOME

echo $TAFJ_HOME
echo $T24_HOME
echo $JAVA_HOME
echo $JBOSS_HOME

# Runs the usage content.
usage ()
{
    info 'Usage : t24dataDeployment.sh Customer customer-l3-data'
    exit
}

function t24_triggerDeployment(){
    if [ "$#" -ne 2 ]
    then
        usage
    fi

    modulename=$1
    nexus_artifact_name=$2
    common_lib_name=$(echo $nexus_artifact_name | rev | cut -d '-' -f2- | rev)
    deploy_env_suffix=$(echo -n $nexus_artifact_name | tail -c 2 | tr '[:lower:]' '[:upper:]')
    lib_name=$(echo $modulename | tr '[:upper:]' '[:lower:]')

    #Trigger assign_Permission function
    assign_Permission

    #Trigger Cleanup_Undeployed_T24Modules function
    Cleanup_Undeployed_T24Modules

    #Trigger T24PackageDataInstaller function
    T24PackageDataInstaller

    #Copy the JAR file to DSPackageUpdates folder
    copyt24Lib $lib_name

    #Make entry in module.xml file
    jboss_module_xml_change $lib_name

    #Trigger packageDataInstaller function
    packageDataInstaller

    #Trigger deploymentStatusCheck function
    #deploymentStatusCheck
    # Trigger Start_JBossAS function
    # Start_JBossAS

    #Trigger revert_Permission function
    revert_Permission
}

#Function to Cleanup undeployed T24 modules before triggering deployment
function Cleanup_Undeployed_T24Modules() {
    echo "-----------------------------------------------------"
    info "Removing old artifacts from directory before download"
    echo "------------------------------------------------------"

    cd $T24_HOME
    rm -rf REL.R21_T24_*
}

#Function to deploy t24 component using T24PackageDataInstaller
function T24PackageDataInstaller() {
    echo "----------------------------------------------------"
    info "Deploy t24 component using T24PackageDataInstaller"
    echo "----------------------------------------------------"

    cd $T24_LOCALLIB_PATH
    mv $nexus_artifact_name.jar $T24_DEPLOY_PATH

    cd $T24_DEPLOY_PATH
    mv $nexus_artifact_name.jar R21_T24_${modulename}-${deploy_env_suffix}_1_JAVA_19.0.jar
    mv $common_lib_name.jar R21_T24_${modulename}_1_JAVA_19.0.jar
    chmod -R 775 R21_T24_${modulename}_1_JAVA_19.0.jar
    chmod -R 775 R21_T24_${modulename}-${deploy_env_suffix}_1_JAVA_19.0.jar

    cd /tmp && mkdir -p deployment_logs
    cd /tmp/deployment_logs && rm -rf ${modulename}_deployment_log.properties
    cd $TAFJ_HOME/bin
    export T24_HOME=/t24disk/R21MB/bnk/UD
    export JAVA_HOME=/t24disk/R21MB/java/jdk1.8.0_291
    export TAFJ_HOME=/t24disk/R21MB/TAFJ
    export PATH=$TAFJ_HOME/bin:$PATH:/t24disk/R21MB/lib/t24lib:$TAFJ_HOME/lib:$TAFJ_HOME/ext

    sh tRun T24PackageInstaller > /tmp/deployment_logs/${modulename}_deployment_log.properties
    cd /tmp/deployment_logs && cat ${modulename}_deployment_log.properties | grep "Success:Update"
    if [ $? -ne 0 ]
    then
        cd /tmp/deployment_logs && cat ${modulename}_deployment_log.properties
        error "Deployment Failed.Please check log for More Details"
        exit 1
    fi
}

#Function to copy T24 JAR Libraries to JBoss Application Server before executing packageDataInstaller
function copyt24Lib() {
    cd $T24_LIB_PATH && cp $T24_HOME/DSPackageUpdates/t24_${lib_name}.jar .
    chmod 755 t24_${lib_name}.jar
}

#Function to add t24 l3 jars as an additional resource root in the jboss module.xml file
function jboss_module_xml_change() {
    cd $JBOSS_HOME/modules/com/temenos/t24/main && grep $t24_{lib_name}.jar module.xml
    if [ $? -ne 0 ]
    then
    echo "Adding t24_${lib_name}.jar as an additional resource root in the jboss module.xml file"
    sed -i "/<\/resources>/i <resource-root path=\"./locallib\/t24_${lib_name}.jar\"\/>" module.xml
    echo "t24_${lib_name}.jar has been added as an additional resource root in the jboss module.xml file"
    else
    echo "Entry for t24_${lib_name}.jar is already available in module.xml file"
     fi
}

#Function to deploy t24 data record using packageDataInstaller
function packageDataInstaller() {
    echo "----------------------------------------------------"
    info "Deploy t24 data record using packageDataInstaller"
    echo "----------------------------------------------------"

    cd $TAFJ_HOME/bin
    export T24_HOME=/t24disk/R21MB/bnk/UD
    export JAVA_HOME=/t24disk/R21MB/java/jdk1.8.0_291
    export TAFJ_HOME=/t24disk/R21MB/TAFJ
    export PATH=$TAFJ_HOME/bin:$PATH:/t24disk/R21MB/lib/t24lib:$TAFJ_HOME/lib:$TAFJ_HOME/ext

    sh tRun packageDataInstaller
}

#Function to assign permission
function assign_Permission() {
    chmod -R 775 /t24disk/R21MB/TAFJ
}

#Function to revert permission
function revert_Permission() {
    chmod -R 755 /t24disk/R21MB/TAFJ
}

#Calling t24_triggerDeployment function
t24_triggerDeployment $1 $2