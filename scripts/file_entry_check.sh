#!/bin/bash
#### Name:      file_entry_check.sh
#### Date:      2021-09-10
#### Version:   1.0.0
#### Author :   Subbu
#### Description:  Script to to add t24 l3 java jars as an additional resource root in the jboss module.xml file

cd /t24disk/R21MB/jboss-eap-7.2/modules/com/temenos/t24/main && grep ${1}.jar module.xml
if [ $? -ne 0 ]
then
echo "Adding ${1}.jar as an additional resource root in the jboss module.xml file"
sed -i "/<\/resources>/i <resource-root path=\"./locallib\/$1.jar\"\/>" module.xml
echo "${1}.jar has been added as an additional resource root in the jboss module.xml file"
else
echo "Entry for ${1}.jar is already available in module.xml file"
fi
