#########################################################################################################################
###############################Steps for setting up JBoss system.service:-###############################################

1. Modify the paths for the JAVA_HOME, TAFJ_HOME, JBOSS_HOME in the jboss_start.sh script and jboss_stop.sh script.
2. Place the scripts for JBOSS start and stop in JBOSS_HOME/bin.
3. Give the file permissions of 775 by running the command
		 chmod 775 script_name
4. Place the jboss.service file in the path
		 /etc/systemd/system/jboss.service
5. Reload the daemons by running the command
		 sudo systemctl daemon-reload
6. Start the service by running the command
		 sudo systemctl start jboss
7. Enable autorun of the service file on VM startup by
		 sudo systemctl enable jboss
8. For checking the logs run the command
		 journalctl -u jboss

##########################################################################################################################