#!/bin/bash
#### Name:      jboss_stop.sh
#### Date:      2021-09-10
#### Version:   1.0.0
#### Author :   Subbu
#### Description:  Script to stop JBOSS based on current running status

# set java_home
export JAVA_HOME=/t24disk/R21MB/java/jdk1.8.0_291/

# set tafj_home
export TAFJ_HOME=/t24disk/R21MB/TAFJ

#Defining JBOSS_HOME
JBOSS_HOME=/t24disk/R21MB/jboss-eap-7.2

#---Check to see if JBOSS is already running - #
cd $JBOSS_HOME/bin
JbossRunningStatus=`ps -ef | grep jboss | grep -v "grep" | grep -v jboss_stop.sh | grep -v "jboss_stop.sh"`
Result=$?
if [ $Result -eq 0 ]
then
        echo "stopping jboss"
        ps -ef | grep jboss | grep -v "grep" | grep -v jboss_stop.sh | grep -v "jboss_stop.sh" | awk '{print $2}' | xargs kill -9
        echo "stopped jboss"
else
        echo "jboss is not running"
fi