#!/usr/bin/env groovy

/**
 * Groovy script to deploy latest/specified artifact to target instances using jenkins pipeline
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
    def appName_lower = sh ( script:"echo ${config.appName}", returnStdout: true).trim().toString().toLowerCase()
    def deploy_env_suffix = sh ( script:"echo -n ${config.libName} | tail -c 2", returnStdout: true).trim().toString().toUpperCase()
    echo "${deploy_env_suffix}"
    if("${deploy_env_suffix}" == "US" || "${deploy_env_suffix}" == "CH"){
        Deploy_Env = "Dev-"+"${deploy_env_suffix}"
    }
    else
    {
         Deploy_Env = "Dev"
    }

    echo "${Deploy_Env}"

    script{
        if ("${Nexus_Artifact_Version}" == "latest" && "${redeploy}" == "true"){
            echo "============================================================================================================"
            echo "Changes in ${common_repo_name} is true so we have to redeploy the ${common_repo_name}.jar along with ${config.libName}.jar"
            echo "============================================================================================================"
            sshPublisher(
                continueOnError: false, failOnError: true,
                publishers: [
                    sshPublisherDesc(
                        configName: "${Deploy_Env}", 
                        transfers: [
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./jboss_stop.sh; sleep 30"
                            ),
                            sshTransfer(
                                cleanRemote: false,
                                sourceFiles: "${common_repo_name}.jar,${config.libName}.jar",
                                removePrefix: "",
                                remoteDirectory: "",
                                execCommand: ""
                            ),
                            sshTransfer(
                                execCommand: "if [ -f ${T24_LOCALLIB_PATH}/t24_${appName_lower}.jar ]; then rm ${T24_LOCALLIB_PATH}/t24_${appName_lower}.jar; fi"
                            ),
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./t24DataDeployCommon.sh ${config.appName} ${config.libName}; ./jboss_start.sh"
                            )
                            ],
                            usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: true
                    )
                ]
            )
        }
        else if("${Nexus_Artifact_Version}" == "latest" && "${redeploy}" == "false"){
            echo "============================================================================================================"
            echo "Changes in ${common_repo_name} is false so we will deploy only ${config.libName}.jar"
            echo "============================================================================================================"
            sshPublisher(
                publishers: [
                    sshPublisherDesc(
                        configName: "${Deploy_Env}", 
                        transfers: [
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./jboss_stop.sh; sleep 30"
                            ),
                            sshTransfer(
                                cleanRemote: false,
                                sourceFiles: "${config.libName}.jar",
                                removePrefix: "",
                                remoteDirectory: "",
                                execCommand: ""
                            ),
                            sshTransfer(
                                execCommand: "if [ -f ${T24_LOCALLIB_PATH}/t24_${appName_lower}.jar ]; then rm ${T24_LOCALLIB_PATH}/t24_${appName_lower}.jar; fi"
                            ),
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./t24DataDeploy.sh ${config.appName} ${config.libName}; ./jboss_start.sh"
                            )
                            ],
                            usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: true
                    )
                ]
            )
        }
    }
}