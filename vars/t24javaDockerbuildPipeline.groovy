#!/usr/bin/env groovy

/**
 * Pipeline as code to automate T24 Build Process
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */
 
/* Read configuration from project jenkinsfile and build.yaml file */
def loadValuesYaml(Map buildParam){
  def config = readYaml (file: "${buildParam?.configPath}")
  return config;
}

def call(Map buildParam) {
	def agentBox;
    def config;
	/* To be determined dynamically */
    agentBox = "${buildParam?.node}"
        if(agentBox == 'null') {
            agentBox = 'master'
        }
    print "Started Executing from node " + agentBox
    
    pipeline {
       /**
		* Run everything on an existing agent configured with a label 'corebanking-agent1'.
        * This agent will need maven, git and a jdk installed at a bare minimum.
        */
		// agent { node { label "${agentBox}" }}
        agent any
		
		/**
		triggers {
        cron(env.BRANCH_NAME == 'develop' ? 'H 3 * * *' : '')
       }*/
		
		/**The options directive is for configuration that applies to the whole job */
		
        options{
			/* Only keep the 10 most recent builds. */
            buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '10',numToKeepStr: '10'))
            /* using the Timestamper plugin we can add timestamps to the console log  */
			//getTimeStamp()
            //retry(3)
			/* we'd really like to be sure that this build doesn't hang forever, so let's time it out after 20 minutes */
            timeout time:20, unit:'MINUTES'
        }
		/**
		parameters { 
            string(name: 'Nexus_Artifact_Version', defaultValue: 'latest', description: 'Enter the artifact version number to deploy') 
        }*/
		
        stages{
            stage ('Initialize & Load Variables'){
                steps{
                    script{
                        config = loadValuesYaml(buildParam)
                    }
                }
            }
			
						            
            stage("Prepare") {
                steps {
                    bitbucketStatusNotify(buildState: 'INPROGRESS', repoSlug: "${config.libName}", commitId: env.GIT_COMMIT)
                }
            }

			// Checkout sourcecode from bitbucket remote repository
			stage('Checkout sourcecode')
            {
                steps{
                    scmCheckout()
                }
            }
			
            // stage('Update POM Version')
            // { 
			//   /* Execute the stage when the specified Groovy expression evaluates to true */
			//   when {
			// 	branch 'release/*'  //only run these steps on the release branch
            //    }
            //     steps{
            //         updateVersion(config)
            //     }
            // }
			

			// Docker Build T24 Image
			stage('Docker Build T24 Image')
            {
                steps{
					buildDockerContainer(config)
                }
            }
			
			// Run the Static Code Analysis
		
            // stage('Sonar scan execution')
            // {
            //     steps{
			// 		sonarScan(config)
            //     }
            // }

            // Run the Quality Gate check
            // stage('Sonar scan Quality Gate check')
            // {
            //     steps{
			// 		sonarQualityGate(config)
            //     }
            // }

			//  Create the release version then create a tage with it
			/*
            stage('T24 Release Tagging')
            {
                when {
                    expression {
                        return env.ReleasedVersion != env.NewVersion;
                    }
                }
                steps{
                    scmTagging(config)
                }
            }*/
			
			/**
			 * Push latest build image to ECR
			 */
            stage('Push to GCR')
            {
                steps{
					pushToRegistry(config)
                }
            }

            /**
			 * Update the latest image in Kubernetes
			 */
            stage('Update/Deploy the Image in Kubernetes')
            {
                steps{
					deployk8s(config)
                }
            }
			
	}
    /**
     * post section defines actions which will be run at the end of the Pipeline run or stage
     * post section condition blocks: always, changed, failure, success, unstable, and aborted
     */
	 
	 post {
	    /* Run regardless of the completion status of the Pipeline run */
	   always {
            jiraSendBuildInfo site: 't24-fs-bdf-coe.atlassian.net'
            // sh "docker rmi -f \$(docker images -aq)"
            sh "yes Y | docker image prune -a"
       }
       /* Only run the steps if the current Pipeline’s or stage’s run has a "success" status */
        success{
                bitbucketStatusNotify(buildState: 'SUCCESSFUL', repoSlug: "${config.libName}", commitId: env.GIT_COMMIT)
            }
		/* Only run the steps if the current Pipeline’s or stage’s run has a "failure" status */
		failure{
                bitbucketStatusNotify(buildState: 'FAILED', repoSlug: "${config.libName}", commitId: env.GIT_COMMIT)
          }

      }
    }
}