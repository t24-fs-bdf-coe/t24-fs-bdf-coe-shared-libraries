#!/usr/bin/env groovy

/**
 * Groovy script to get the change history of the given file or directory from SCM
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */
// get change log to be send over the mail or MS team chat
def String call() {
  MAX_MSG_LEN = 512
  def changeString = ""
  def changeLogSets = currentBuild.changeSets
  for (int i = 0; i < changeLogSets.size(); i++) {
     def entries = changeLogSets[i].items
     for (int j = 0; j < entries.length; j++) {
         def entry = entries[j]
         truncated_msg = entry.msg.take(MAX_MSG_LEN)
         changeString += " - ${truncated_msg} [${entry.author}]\n"
     }
  }
  if (!changeString) {
     changeString = "No changes"
  }
  return changeString
}
return this


