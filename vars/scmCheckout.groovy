#!/usr/bin/env groovy

/**
 * Groovy script to checkout remote Git repository
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
	checkout([
		$class: 'GitSCM', 
		branches: scm.branches,
		// extensions: scm.extensions + [[$class: 'CleanCheckout']], [[$class: 'LocalBranch', localBranch: 'localBranch']],
		userRemoteConfigs: scm.userRemoteConfigs
	])
}
