#!/usr/bin/env groovy

/**
 * Groovy script to execute T24 Java Build using Apache Maven
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
  def execute = "${config.t24Build.required}"
  echo "Execute: ${execute}"
  // def maven = environmentProperties.maven
	if (execute != 'null' && execute != 'false') {
			withEnv(["PATH+MAVEN=${tool 'maven-coe'}/bin"] ) {
				sh "mvn ${config.t24Build.command} -s settings.xml"
		}
	}
}

