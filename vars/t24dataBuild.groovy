#!/usr/bin/env groovy

/**
 * Groovy script to execute T24 Data Build using Apache Maven
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
  script {
    sh "cd ${T24_LIBRARIES_PATH}/${config.t24relVersion}/TAFJ/conf && sed -ie 's|^temn.tafj.directory.precompile.*|temn.tafj.directory.precompile=${T24_LIBRARIES_PATH}/${config.t24relVersion}/t24lib:${T24_LIBRARIES_PATH}/${config.t24relVersion}/TAFJ/lib|g' tafj.properties"
    sh "cp -n ${T24_LIBRARIES_PATH}/${config.t24relVersion}/TAFJ/JbcSonarPlugin/JbcSonarPlugin.jar ${T24_LIBRARIES_PATH}/${config.t24relVersion}/TAFJ/ext/JbcSonarPlugin.jar || true"
    sh "cp -n ${T24_LIBRARIES_PATH}/${config.t24relVersion}/TAFJ/JbcSonarPlugin/JBC_Precompiler.jar ${T24_LIBRARIES_PATH}/${config.t24relVersion}/TAFJ/ext/JBC_Precompiler.jar || true"
    withEnv(["PATH+MAVEN=${tool 'bpi-phoenix-maven'}/bin"] ) {
      echo "$MAVEN_HOME"
      echo "$JAVA_HOME"
      sh "java -version"
      sh "mvn -o -f t24-${config.appName}-packager/module/pom.xml \
          -Dds.ignoreValidationErrors=true \
          -DinsertDir=${T24_LIBRARIES_PATH}/${config.t24relVersion}/t24lib \
          -Dlib.dir=${T24_LIBRARIES_PATH}/${config.t24relVersion}/t24lib \
          -Dtemn.tafj.compiler.naming.strict.mode=false \
          -Dtemn.tafj.compiler.component.strict.mode=false \
          -Denv.TAFJ_HOME=${T24_LIBRARIES_PATH}/${config.t24relVersion}/TAFJ clean package"
    }
  }
}

