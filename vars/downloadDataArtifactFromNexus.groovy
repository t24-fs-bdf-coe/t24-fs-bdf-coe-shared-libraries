#!/usr/bin/env groovy

/**
 * Groovy script to download latest/specified artifact from nexus using jenkins pipeline
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config){
    // Read POM xml file using 'readMavenPom' step 
    appname = sh ( script:"echo ${config.appName}", returnStdout: true).trim().toString().toLowerCase()
    pom = readMavenPom file: "t24-${appname}-packager/pom.xml";
    groupId = "${config.groupId}"
    artifactId= "${config.artifactId}"
    baseVersion = "${pom.version}"
	withCredentials([usernamePassword(credentialsId: "${config.nexus.credentialId}", passwordVariable: 'NEXUS_PASS', usernameVariable: 'NEXUS_USER')]) {
	    script{
		// Downloading specified artifact through Curl from Nexus3
        if("${Nexus_Artifact_Version}" != "latest"){
            echo "============================================================================================================"
            echo "Downloading the ${config.libName}-${Nexus_Artifact_Version} file for deployment."
            echo "============================================================================================================"
            sh "sudo curl -L -u ${NEXUS_USER}:'${NEXUS_PASS}' -X GET 'https://${NEXUS_URL}/service/rest/v1/search/assets/download?sort=version&repository=${config.nexus.repository}&maven.groupId=${groupId}&maven.artifactId=${artifactId}&maven.baseVersion=${baseVersion}&maven.extension=jar&version=${Nexus_Artifact_Version}' -H 'accept: application/json' -o ${config.libName}.jar"
        }
        else{
            script{
                if ("${redeploy}" == "true"){
                    echo "============================================================================================================"
                    echo "Since there are changes in ${common_repo_name} files we need to download the latest jar file for deployment."
                    echo "============================================================================================================"
                    sh "sudo curl -L -u ${NEXUS_USER}:'${NEXUS_PASS}' -X GET 'https://${NEXUS_URL}/service/rest/v1/search/assets/download?sort=version&repository=${config.nexus.repository}&maven.groupId=${groupId}&maven.artifactId=${common_repo_name}&maven.baseVersion=${baseVersion}&maven.extension=jar' -H 'accept: application/json' -o ${common_repo_name}.jar"
                }
				// Downloading latest artifact through Curl from Nexus3
                echo "============================================================================================================"
                echo "Downloading the latest version of ${config.libName} file for deployment."
                echo "============================================================================================================"
                sh "sudo curl -L -u ${NEXUS_USER}:'${NEXUS_PASS}' -X GET 'https://${NEXUS_URL}/service/rest/v1/search/assets/download?sort=version&repository=${config.nexus.repository}&maven.groupId=${groupId}&maven.artifactId=${artifactId}&maven.baseVersion=${baseVersion}&maven.extension=jar' -H 'accept: application/json' -o ${config.libName}.jar"
                echo "Latest artifact is present in target folder as ${config.libName}.jar"
           }
		}
      }
   }
}