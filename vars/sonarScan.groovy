#!/usr/bin/env groovy

/**
 * This script enables static code analysis of T24 application using SonarQube.
 * It leverages in-built java rules to raise issues against T24 java code
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */
def call(Map config) {
	def execute = "${config.sonarScan?.required}"
	def skipAnalysis = "${params.'Skip Sonar Analysis'}"
	
	if (skipAnalysis == 'true') {
		return;
	}
	
	if (execute != 'null' && execute != 'false') {
		withSonarQubeEnv('SonarQube') {
			withEnv(["PATH+MAVEN=${tool 'maven-coe'}/bin"] ) {
				sh "mvn ${config.sonarScan.command}"
				}
			}
		echo "Running Sonar Scan"
		timeout(time: 10, unit: 'MINUTES') {
			echo "Response from sonar"
		}
	}
}