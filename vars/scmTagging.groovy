#!/usr/bin/env groovy

/**
 * Groovy script to Automate Tag Creation on Bitbucket
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config){
    script {
		// Get git remote url
        env.GitUrl = sh (script: 'git config --get remote.origin.url | awk -F"https:\\/\\/" "{print \\$2}"', returnStdout: true).trim()
    }
    withCredentials([usernamePassword(credentialsId: "${config.git.credentialId}", usernameVariable: 'GIT_USER', passwordVariable: 'GIT_PASS')]) {
        script {
            env.encodedPass=URLEncoder.encode(GIT_PASS, "UTF-8")
        }
		// Configure your DVCS username for commits 
        sh "git config user.name '${config.git.username}'"
        sh "git config user.email '${config.git.userEmail}'"
		// Add file contents to the index
        sh "git add ."
		// Record changes to the repository
        sh "git commit -m 'Update version to $NewVersion'"
		// Creating tags to capture a point in history that is used for a marked version release
        sh "git tag -a $NewVersion -m 'Version updated to $NewVersion'"
	    // Push git changes to remote repo
        sh "git push https://${GIT_USER}:${encodedPass}@${GitUrl} develop --tags"
    }
}
