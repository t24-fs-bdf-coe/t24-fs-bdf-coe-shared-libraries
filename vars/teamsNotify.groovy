#!/usr/bin/env groovy

/**
 * Groovy script to send jenkins jobs status notifications to Microsoft Teams
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

import groovy.json.JsonOutput
def call(Map config) {
    script{
        sh """
        curl --request POST \
        --url ${config.teamsWebhook} \
        --header 'content-type: application/json' \
        --data '{"@type": "MessageCard","@context": "http://schema.org/extensions","themeColor": "0076D7","summary": "New Build Started","sections": [{"activityTitle": "New Build Started","activityImage": "https://wiki.jenkins.io/download/attachments/2916393/logo.png","facts": [{"name":"Build #","value":"'"$BUILD_NUMBER"'"},{"name": "App_Name", "value": "'"$JOB_NAME"'"},{"name": "Branch","value": "'"$GIT_BRANCH"'"}, {"name": "Commit","value": "'"$GIT_COMMIT"'"}, {"name": "URL","value": "'"$BUILD_URL"'"}],"markdown": true}]}'
        """
    }
}


