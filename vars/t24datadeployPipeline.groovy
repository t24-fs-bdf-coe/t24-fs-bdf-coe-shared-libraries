#!/usr/bin/env groovy

/**
 * Pipeline as code to automate T24 deployment Process
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */
 
/* Read configuration from project jenkinsfile and deploy.yaml file */
def loadValuesYaml(Map buildParam){
  def config = readYaml (file: "${buildParam?.configPath}")
  return config;
}

def call(Map buildParam) {
	def agentBox;
    def config;
    def git_commit_id;
    def git_last_commit;
    def redeploy;
    def common_repo_name;
	/* To be determined dynamically */
    agentBox = "${buildParam?.node}"
        if(agentBox == 'null') {
            agentBox = 'master'
        }
    print "Started Executing from node " + agentBox
    
    pipeline {
       /**
		* Run everything on an existing agent configured with a label 'corebanking-agent1'.
        * This agent will need maven, git and a jdk installed at a bare minimum.
        */
		agent { node { label "${agentBox}" }}
		
		/**
		triggers {
        cron(env.BRANCH_NAME == 'develop' ? 'H 3 * * *' : '')
       }*/
		
		/**The options directive is for configuration that applies to the whole job */
		
        options{
			/* Only keep the 10 most recent builds. */
            buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '10',numToKeepStr: '10'))
            /* using the Timestamper plugin we can add timestamps to the console log  */
			//getTimeStamp()
            //retry(3)
			/* we'd really like to be sure that this build doesn't hang forever, so let's time it out after 20 minutes */
            timeout time:20, unit:'MINUTES'
        }
		
		parameters { 
            // choice(choices: 'Dev\nDev-CH\nDev-US', description: 'Select an environment to deploy from the drop-down', name: 'Deploy_Env')
            string(name: 'Nexus_Artifact_Version', defaultValue: 'latest', description: 'Please provide the version number that need to be deployed')
        }
		
        stages{
            stage ('Initialize & Load Variables'){
                steps{
                    script{
                        config = loadValuesYaml(buildParam)
                    }
                }
            }
			
						            
		   stage("Prepare") {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS', repoSlug: "${config.libName}", commitId: env.GIT_COMMIT)

            }
          }
			// Checkout sourcecode from bitbucket remote repository
			stage('Checkout sourcecode')
            {
                steps{
                    scmDeployCheckout(config)
                }
            }
		
		
			/**
			 * Download the artifacts from nexus repository for the deployment script to pick it up and deploy them into target server
			 */
            stage('Nexus Download')
            {
                steps{
					downloadDataArtifactFromNexus(config)
                }
            }
			
			
            stage('Trigger Deployment')
            {
                steps{
					t24DataDeploy(config)
                }
            }

	}
    /**
    * post section defines actions which will be run at the end of the Pipeline run or stage
    * post section condition blocks: always, changed, failure, success, unstable, and aborted
    */
	 
	post {
	    /* Run regardless of the completion status of the Pipeline run */
	   always {
           jiraSendBuildInfo site: 'phoenix24.atlassian.net'
        }
       /* Only run the steps if the current Pipeline’s or stage’s run has a "success" status */
        success{
                bitbucketStatusNotify(buildState: 'SUCCESSFUL', repoSlug: "${config.libName}", commitId: env.GIT_COMMIT)
            }
		/* Only run the steps if the current Pipeline’s or stage’s run has a "failure" status */
		failure{
                bitbucketStatusNotify(buildState: 'FAILED', repoSlug: "${config.libName}", commitId: env.GIT_COMMIT)
            }

        }
    }
}