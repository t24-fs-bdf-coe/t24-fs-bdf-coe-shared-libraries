#!/usr/bin/env groovy

/**
 * Groovy script to download latest/specified artifact from nexus using jenkins pipeline
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config){
    // Read POM xml file using 'readMavenPom' step 
    pom = readMavenPom file: "Container/pom.xml";
    groupId = "com.bpi.t24"
    artifactId= "${config.appName}"
    baseVersion = "${pom.version}"
	withCredentials([usernamePassword(credentialsId: "${config.nexus.credentialId}", passwordVariable: 'NEXUS_PASS', usernameVariable: 'NEXUS_USER')]) {
	    script{
		// Downloading specified artifact through Curl from Nexus3
        if("${Nexus_Artifact_Version}" != "latest"){
            echo "============================================================================================================"
            echo "Downloading the ${config.libName}-${Nexus_Artifact_Version} file for deployment."
            echo "============================================================================================================"
            sh "sudo curl -L -u ${NEXUS_USER}:'${NEXUS_PASS}' -X GET 'https://${NEXUS_URL}/service/rest/v1/search/assets/download?sort=version&repository=${config.nexus.repository}&maven.groupId=${groupId}&maven.artifactId=${artifactId}&maven.baseVersion=${baseVersion}&maven.extension=war&version=${Nexus_Artifact_Version}' -H 'accept: application/json' -o ${config.libName}-${Nexus_Artifact_Version}.war"
        }
        else{
            script{
				// Downloading latest artifact through Curl from Nexus3
                echo "============================================================================================================"
                echo "Downloading the latest version of ${config.libName} file for deployment."
                echo "============================================================================================================"
                sh "sudo curl -L -u ${NEXUS_USER}:'${NEXUS_PASS}' -X GET 'https://${NEXUS_URL}/service/rest/v1/search/assets/download?sort=version&repository=${config.nexus.repository}&maven.groupId=${groupId}&maven.artifactId=${artifactId}&maven.baseVersion=${baseVersion}&maven.extension=war' -H 'accept: application/json' -o ${config.libName}.war"
                echo "Latest artifact is present in target folder as ${config.libName}.war"
           }
		}
      }
   }
}