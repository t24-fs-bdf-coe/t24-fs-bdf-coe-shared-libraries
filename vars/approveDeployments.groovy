#!/usr/bin/env groovy

/**
 * Groovy script to trigger approval for automated deployment
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */
 
def call(Map config) {
    timeout(time: 10, unit: 'MINUTES') { // timeout waiting for input after 10 minutes
    
    script {
        // capture the approval details in approvalMap.					
		approvalMap = input id: 'deployment', message: 'Are you prepared to DEV deploy?', ok: 'Approve?', parameters: [choice(choices: 'Dev1\nDev2\nDev3', description: 'Select an environment to deploy from the drop-down', name: 'ENVIRONMENT'),string(name: 'VERSION', defaultValue: 'latest', description: 'Please provide the version number that need to be deployed')], submitter: "$DEVOPS_APPROVER", submitterParameter: 'APPROVER'						
        // Approval for the automated deployment
        echo "Deployment to ${approvalMap['ENVIRONMENT']} was approved by: ${approvalMap['APPROVER']}"
        env.TARGETENV = "${approvalMap['ENVIRONMENT']}"
		env.Nexus_Artifact_Version = "${approvalMap['VERSION']}"

        }
    }
}
