#!/usr/bin/env groovy

/**
 * Groovy script for Publishing Artifacts to Sonatype Nexus using Jenkins Pipelines
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
    // Read POM xml file using 'readMavenPom' step 
    pom = readMavenPom file: "pom.xml";
    // Find built artifact under target folder
    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
    // Print some info from the artifact found
    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
    // Extract the path from the File found
    artifactPath = filesByGlob[0].path;
    // Assign to a boolean response verifying If the artifact name exists
    artifactExists = fileExists artifactPath;
    if(artifactExists) {
        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";

        nexusArtifactUploader(
            nexusVersion: "${NEXUS_VERSION}",
            protocol: "${NEXUS_PROTOCOL}",
            nexusUrl: "${NEXUS_URL}",
            groupId: "${pom.groupId}",
            version: "${pom.version}",
            repository: "${config.nexus.repository}",
            credentialsId: "${config.nexus.credentialId}",
            artifacts: [
                // Artifact generated such as .jar, .war and .zip files.
                [
                    artifactId: "${pom.artifactId}",
                    classifier: "",
                    file: "${artifactPath}",
                    type: "${pom.packaging}",
                ]

                // Lets upload the pom.xml file for additional information for Transitive dependencies
                   /** [
                       artifactId: "${pom.artifactId}",
                       classifier: "",
                       file: "pom.xml",
                       type: "pom"
                    ]*/
            ]
        );

    } else {
        error "*** File: ${artifactPath}, could not be found";
    }
}
