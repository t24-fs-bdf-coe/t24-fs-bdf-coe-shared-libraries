#!/usr/bin/env groovy

/**
 * Groovy script to checkout remote Git repository
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
	checkout([
		$class: 'GitSCM', 
		branches: scm.branches,
		// extensions: scm.extensions + [[$class: 'CleanCheckout']], [[$class: 'LocalBranch', localBranch: 'localBranch']],
		userRemoteConfigs: scm.userRemoteConfigs
	])
	
	withCredentials([usernamePassword(credentialsId: "${config.git.credentialId}", passwordVariable: 'GIT_PASS', usernameVariable: 'GIT_USER')]) {
		script{
			env.common_repo_name = sh ( script:"echo ${config.libName} | rev | cut -d '-' -f2- | rev", returnStdout: true).trim()
            // sh 'echo test > GIT_LAST_COMMIT_ID'
			env.redeploy = false;
			env.encodedPass=URLEncoder.encode(GIT_PASS, "UTF-8")
			env.git_commit_id = sh ( script:"git ls-remote https://${GIT_USER}:${encodedPass}@bitbucket.org/bpi-phoenix/${common_repo_name}.git HEAD | awk '{print \$1}'", returnStdout:true).trim()
			env.git_last_commit = sh ( script:"cat GIT_LAST_COMMIT_ID", returnStdout: true).trim()
			echo "Git commit ID: ${git_commit_id}"
			echo "Git Last commit ID: ${git_last_commit}"
			if ("${git_commit_id}" != "${git_last_commit}"){
				env.redeploy = true;
			}
			sh 'echo "${git_commit_id}" > GIT_LAST_COMMIT_ID'

			echo "============================================================================================================"
            echo "Changes in ${common_repo_name}? Need to redeploy the ${common_repo_name}.jar file? : ${env.redeploy}"
            echo "============================================================================================================"
			
		}
	}
}
