#!/usr/bin/env groovy

def call() {
    sh "sudo aws ecr get-login-password --region ${AWS_REGION} | docker login --username AWS --password-stdin "$ECR_REGISTRY""
}