#!/usr/bin/env groovy

/**
 * Groovy script to update project version in pom.xml file following the semantic versioning
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config){
    
    def jobnameparts = JOB_NAME.tokenize('/') as String[]
	// Get the current jenkins job.
    def jobname = jobnameparts[0]
	def jobbasename = "${jobname}_${env.BRANCH_NAME}".
	// Use Pipeline Utility Steps plugin to read information from pom.xml into env variables
	pom = readMavenPom file: "pom.xml";
    env.ReleasedVersion = pom.version
	// Change environment to the develop branch
    sh 'git checkout develop'
    script {
		// Get git remote url
        env.GitUrl = sh (script: 'git config --get remote.origin.url | awk -F"https:\\/\\/" "{print \\$2}"', returnStdout: true).trim()
    }
    withCredentials([usernamePassword(credentialsId: "${config.git.credentialId}", usernameVariable: 'GIT_USER', passwordVariable: 'GIT_PASS')]) {
        script {
            env.encodedPass=URLEncoder.encode(GIT_PASS, "UTF-8")
        }
        sh 'git reset --hard origin/develop'
        sh 'git pull https://${GIT_USER}:${encodedPass}@${GitUrl} develop --force'
    }
    echo "${ReleasedVersion}"
	echo "${jobname}"
	echo "${env.BRANCH_NAME}"
	env.jname = "${jobbasename}"
	echo "${jname}"
    script {
		/**
        * Get commit message for given commit reference
        */
        def commit_msg = sh (script: 'git log -1 --pretty=%B', returnStdout: true)
		 if(commit_msg.contains("Merged")){
			env.NewVersion = sh (script: 'bash ../"${jname}"@libs/bpi-t24-shared-libraries/scripts/version.sh ${ReleasedVersion} $(git log -1 --pretty=%B | head -n 3 | tail -n 1)', returnStdout: true).trim()
        }
        else{
            env.NewVersion = sh (script: 'bash ../"${jname}"@libs/bpi-t24-shared-libraries/scripts/version.sh ${ReleasedVersion} $(git log -1 --pretty=%B)', returnStdout: true).trim()
        }
    }
    sh 'echo New Version is: $NewVersion'

    if("${ReleasedVersion}" != "${NewVersion}")
	    // Using the Pipeline Maven plugin we can set maven configuration settings, publish test results, and annotate the Jenkins console
        withEnv(["PATH+MAVEN=${tool 'bpi-phoenix-maven'}/bin"] ) {
			// To update the version numbers in project pom.xml files before tagging
            sh 'mvn versions:set versions:commit -DnewVersion="$NewVersion"'
        }
}