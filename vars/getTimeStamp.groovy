#!/usr/bin/env groovy

/**
 * Groovy Script to get timestamp in CET format
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */
 
def String call() {
   Date date = new Date()
   return date.format('yyyyMMddHHmmss',TimeZone.getTimeZone('CET')) as String
}
return this


