#!/usr/bin/env groovy

/**
 * Groovy script to Mark build as failed in Jenkins,if SonarQube quality checks not met
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
    def props = getProperties("target/sonar/report-task.txt")
    sonarServerUrl=props.getProperty('serverUrl')
    url = new URL(props.getProperty('ceTaskUrl'))
    sonarBasicAuth  = SONAR_TOKEN + ":"
    sonarBasicAuth = sonarBasicAuth.getBytes().encodeBase64().toString()
    // waiting for sonar results based into the configured web hook in Sonar server which push the status back to jenkins
    def ceTask
        timeout(time: 1, unit: 'MINUTES') {  // Just in case something goes wrong, pipeline will be killed after a timeout
            waitUntil {
            ceTask = jsonParse(url, sonarBasicAuth)
            // echo ceTask.toString()
            return "SUCCESS".equals(ceTask["task"]["status"])
          }
        }
        url = new URL(sonarServerUrl + "/api/qualitygates/project_status?analysisId=" + ceTask["task"]["analysisId"])
        def qualitygate =  jsonParse(url, sonarBasicAuth)
        // def status = qualitygate.toString().status
        if ("ERROR".equals(qualitygate["projectStatus"]["status"])) {
          error  "Pipeline aborted due to quality gate failure: "+ qualitygate["projectStatus"]["status"]
        }
        echo  "Sonarqube quality gate pass:"+ qualitygate["projectStatus"]["status"] 
}

// Groovy script to get values from properties file 

def Properties getProperties(filename) {
    def properties = new Properties()
    properties.load(new StringReader(readFile(filename)))
    return properties
}

// Groovy script to read a response from text file and return it as a JSON object

@NonCPS
def jsonParse(text) {
    return new groovy.json.JsonSlurperClassic().parseText(text);
}

// Groovy script to read a response from a URL and return it as a JSON object

@NonCPS
def jsonParse(URL url, String basicAuth) {
    def conn = url.openConnection()
    conn.setRequestProperty( "Authorization", "Basic " + basicAuth )
    InputStream is = conn.getInputStream();
    def json = new groovy.json.JsonSlurperClassic().parseText(is.text);
    conn.disconnect();
    return json
}