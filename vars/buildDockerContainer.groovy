#!/usr/bin/env groovy

def call(Map config) {
    sh "docker build --force-rm -t ${config.ECR_REGISTRY}/${config.projectName}/${config.imageName}:${env.BUILD_NUMBER} ."
}
