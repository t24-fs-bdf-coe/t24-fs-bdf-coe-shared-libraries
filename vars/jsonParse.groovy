#!/usr/bin/env groovy

/**
 * Groovy script to read a response from text file/URL and return it as a JSON object
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(){
    @NonCPS
    def jsonParse(text) {
        return new groovy.json.JsonSlurperClassic().parseText(text);
    }

    @NonCPS
    def jsonParse(URL url, String basicAuth) {
        def conn = url.openConnection()
        conn.setRequestProperty( "Authorization", "Basic " + basicAuth )
        InputStream is = conn.getInputStream();
        def json = new groovy.json.JsonSlurperClassic().parseText(is.text);
        conn.disconnect();
        return json
    }
}