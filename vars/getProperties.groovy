#!/usr/bin/env groovy

/**
 * Groovy script to get values from properties file
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */
def call(){
    def Properties getProperties(filename) {
        def properties = new Properties()
        properties.load(new StringReader(readFile(filename)))
        return properties
    }
}