#!/usr/bin/env groovy

/**
 * Groovy script to execute T24 IRIS API Build using Apache Maven
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
  def execute = "${config.t24IrisBuild.required}"
  echo "Execute: ${execute}"
  // def maven = environmentProperties.maven
	if (execute != 'null' && execute != 'false') {
			withEnv(["PATH+MAVEN=${tool 'bpi-phoenix-maven'}/bin"] ) {
				sh "mvn ${config.t24IrisBuild.command} -U"
		}
	}
}

