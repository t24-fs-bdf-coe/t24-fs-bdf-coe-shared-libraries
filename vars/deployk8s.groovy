#!/usr/bin/env groovy

def call(Map config) {
    // timeout(time: 10, unit: 'MINUTES') { // timeout waiting for input after 10 minutes
    
    // script {
    //     // capture the approval details in approvalMap.					
	// 	approvalMap = input (id: 'deployment', message: 'Are you prepared to deploy?', ok: 'Approve?', submitter: "$DEVOPS_APPROVER", submitterParameter: 'APPROVER')						
    //     // Approval for the automated deployment
    //     echo "Deployment was approved by: ${approvalMap['APPROVER']}"
    //     }
    // }

    script {
        timeout(time: 10, unit: 'MINUTES') {
            userInput = input message: 'Is this deployment approved?', ok: '', submitter: "${DEVOPS_APPROVER}", parameters: [
                [$class: 'hudson.model.ChoiceParameterDefinition', choices: 'Approved\nNot Approved', name: 'isApproved', description: 'This input can only be answered by users with deployment permission on this environment']
            ]
            if (userInput == "Not Approved") {
                error "Deployment to Dev K8s cluster is not approved"
            }
            else
            {
                withEnv(["PATH+GCLOUD_PATH=${tool 'GCLOUD_PATH'}/bin"])
                {
                    sh "gcloud auth activate-service-account ${devServiceAccount} --key-file=${devServiceAccountKeyPath}"
                    sh "gcloud container clusters get-credentials t24app --zone us-central1-c --project ${config.projectName}"
                    sh "kubectl set image deployment/${config.serviceName} ${config.containerName}=${config.ECR_REGISTRY}/${config.projectName}/${config.imageName}:${env.BUILD_NUMBER}"
                    sh "kubectl rollout status -w deployment/${config.serviceName}"
                    sh "kubectl describe deployment ${config.serviceName}"
                }
            }

        }
    }
}