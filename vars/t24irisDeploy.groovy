#!/usr/bin/env groovy

/**
 * Groovy script to deploy latest/specified artifact to target instances using jenkins pipeline
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

def call(Map config) {
    // def deploy_env_suffix = sh ( script:"echo -n ${config.libName} | tail -c 2", returnStdout: true).trim().toString().toUpperCase()
    // echo "${deploy_env_suffix}"
    // Deploy_Env = "Dev-"+"${deploy_env_suffix}"

    // echo "${Deploy_Env}"

    script{
        if ("${Nexus_Artifact_Version}" == "latest"){
            echo "============================================================================================================"
            echo "Deploying ${config.libName}.war"
            echo "============================================================================================================"
            sshPublisher(
                continueOnError: false, failOnError: true,
                publishers: [
                    sshPublisherDesc(
                        configName: "${Deploy_Env}", 
                        transfers: [
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./jboss_stop.sh; sleep 30"
                            ),
                            sshTransfer(
                                execCommand: "if [ -f ${T24_DEPLOYMENT_PATH}/${config.libName}.war ]; then rm ${T24_DEPLOYMENT_PATH}/${config.libName}.war; fi"
                            ),
                            sshTransfer(
                                cleanRemote: false,
                                sourceFiles: "${config.libName}.war",
                                removePrefix: "",
                                remoteDirectory: "",
                                execCommand: ""
                                ),
                            sshTransfer(
                                execCommand: "cd ${T24_LOCALLIB_PATH}; mv ${config.libName}.war ${T24_DEPLOYMENT_PATH}/ ; cd ${T24_DEPLOYMENT_PATH} ; chmod 775 ${config.libName}.war"
                            ),
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./jboss_start.sh"
                            )
                            ],
                            usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: true
                    )
                ]
            )
        }
        else if("${Nexus_Artifact_Version}" != "latest"){
            echo "============================================================================================================"
            echo "Deploying ${config.libName}-${Nexus_Artifact_Version}.war"
            echo "============================================================================================================"
            sshPublisher(
                continueOnError: false, failOnError: true,
                publishers: [
                    sshPublisherDesc(
                        configName: "${Deploy_Env}", 
                        transfers: [
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./jboss_stop.sh; sleep 30"
                            ),
                            sshTransfer(
                                execCommand: "if [ -f ${T24_DEPLOYMENT_PATH}/${config.libName}-${Nexus_Artifact_Version}.war ]; then rm ${T24_DEPLOYMENT_PATH}/${config.libName}-${Nexus_Artifact_Version}.war; fi"
                            ),
                            sshTransfer(
                                cleanRemote: false,
                                sourceFiles: "${config.libName}-${Nexus_Artifact_Version}.war",
                                removePrefix: "",
                                remoteDirectory: "",
                                execCommand: ""
                                ),
                            sshTransfer(
                                execCommand: "cd ${T24_LOCALLIB_PATH}; mv ${config.libName}-${Nexus_Artifact_Version}.war ${T24_DEPLOYMENT_PATH}/ ; cd ${T24_DEPLOYMENT_PATH} ; chmod 775 ${config.libName}-${Nexus_Artifact_Version}.war"
                            ),
                            sshTransfer(
                                execCommand: "cd ${T24_DEPLOY_SCRIPT_PATH}; ./jboss_start.sh"
                            )
                            ],
                            usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: true
                    )
                ]
            )
        }
    }
}