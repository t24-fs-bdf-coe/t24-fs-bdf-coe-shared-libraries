#!/usr/bin/env groovy

def call(Map config) {
    // loginToAWSECRDockerRegistry()
    sh "cat ${devServiceAccountKeyPath} | docker login -u _json_key --password-stdin https://gcr.io"
    sh "docker tag ${config.ECR_REGISTRY}/${config.projectName}/${config.imageName}:${env.BUILD_NUMBER} ${config.ECR_REGISTRY}/${config.projectName}/${config.imageName}:${env.BUILD_NUMBER}"
    sh "docker push ${config.ECR_REGISTRY}/${config.projectName}/${config.imageName}:${env.BUILD_NUMBER}"
}